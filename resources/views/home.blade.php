@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a class="btn btn-success" type="button" href="{{ route('receita.index') }}">Receita</a>
                    <a class="btn btn-danger" type="button" href="{{ route('despesa.index') }}">Despesa</a>
                </div>
            </div>
        </div>
        <table-component></table-component>
    </div>
</div>
@endsection
