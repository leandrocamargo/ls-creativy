@extends('layouts.app')

@section('content')
    <div class="card col-lg-6">
        <div class="card-body">
            <form method="post" action="{{ route('despesa.store') }}">
                @csrf
                <label>Data</label>
                <input class="form-control" type="date" name="data">

                <label>Descrição</label>
                <input class="form-control" type="text" name="descricao">

                <label>Valor</label>
                <input class="form-control" type="text" name="valor">

                <button class="btn btn-success" type="submit">Salvar</button>
            </form>
        </div>
    </div>
@endsection
