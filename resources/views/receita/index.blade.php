@extends('layouts.app')

@section('content')
    <div class="card col-lg-6">
        <div class="card-header">
            <a class="btn btn-sm btn-primary" type="button" href="{{ route('home') }}">
                Home
            </a>
            <a class="btn btn-sm btn-info" type="button" href="{{ route('receita.create') }}">
                Lançar
            </a>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Data</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $receitas as $receita )
                    <tr>
                        <td>{{ date( 'd/m/Y', strtotime($receita->data)) }}</td>
                        <td>{{ $receita->descricao }}</td>
                        <td>{{ $receita->valor }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $receitas->links() }}
        </div>
    </div>
@endsection
