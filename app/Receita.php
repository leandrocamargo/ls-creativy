<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receita extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'receitas';
    protected $fillable = [
        'data',
        'descricao'
    ];
}
