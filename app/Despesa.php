<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Despesa extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'despesas';
    protected $fillable = [
        'data',
        'descricao'
    ];
}
