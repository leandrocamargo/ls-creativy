<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('telefone', 20);
            $table->string('email', 50)->unique();
            $table->string('celular', 50);
            $table->string('operador', 50);
            $table->string('facebook', 100);
            $table->string('instagram', 100);
            $table->string('whatsapp', 100);
            $table->string('linkedin', 100);
            $table->string('twiter', 100);
            $table->string('telegram', 100);
            $table->string('skype', 100);
            $table->boolean('ativo');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
