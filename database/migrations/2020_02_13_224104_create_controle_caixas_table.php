<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControleCaixasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controle_caixas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('data');
            $table->double('valor_inicial');
            $table->double('valor_entrada');
            $table->double('valor_saida');
            $table->double('valor_final');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controle_caixas');
    }
}
